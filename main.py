import json
import boto3
import botocore
import logging


S3 = boto3.resource("s3")
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def make_log(event):
    logger.info(event)

def download_data(bucket, key):
    logger.info("download_data function has been called with the following parameters:" + bucket + " / " + key)
    path = "/tmp/" + key
    res = S3.Bucket(bucket).download_file(key, path)
    return res

def put_s3_data(data, bucket, key):
    logger.info("put_s3_data function called with bucket and key:" + bucket + "/" + key)
    obj = S3.Object(bucket, key)
    return obj.put(Body=data)

def get_s3_data(bucket: str, key: str):
    logger.info("get_s3_data function has been called with the following parameters:" + bucket + " / " + key)
    obj = S3.Object(bucket, key)
    return obj.get()['Body'].read().decode('utf-8')

def http_response(statusCode, body):
    
    logger.info("Response sent from lambda with status code:" + str(statusCode))
    event = {"statusCode":statusCode, "isBase64Encoded" : False, "body": body, "headers": {}}

    return event
    
def doOverlap(p1, p2, c1, c2): 

    dx = min(p2[0], c2[0]) - max(p1[0], c1[0])
    dy = min(p2[1], c2[1]) - max(p1[1], c1[1])

    if (dx>=0) and (dy>=0):
        return True
    
    return False

def get_text_in_zone(paragraphs, coordinates):
    
    cx_min = coordinates[0]
    cy_min = coordinates[1]
    cx_max = coordinates[2]
    cy_max = coordinates[3]

    big_coordinates = paragraphs[0]["boundingPoly"]["vertices"]

    x_min = big_coordinates[0]["x"]
    y_min = big_coordinates[0]["y"]
    x_max = big_coordinates[2]["x"]
    y_max = big_coordinates[2]["y"]

    text = []

    for p in paragraphs[1:]:

        px_min = p["boundingPoly"]["vertices"][0]["x"]
        py_min = p["boundingPoly"]["vertices"][0]["y"]
        px_max = p["boundingPoly"]["vertices"][2]["x"]
        py_max = p["boundingPoly"]["vertices"][2]["y"]

        px_nmin = (px_min - x_min) / (x_max - x_min)
        py_nmin = (py_min - y_min) / (y_max - y_min)
        px_nmax = (px_max - x_min) / (x_max - x_min)
        py_nmax = (py_max - y_min) / (y_max - y_min)

        

        if doOverlap((px_nmin,py_nmin),(px_nmax, py_nmax),(cx_min,cy_min),(cx_max, cy_max)):
            text.append(p["description"])

    return text




def handler(event, context):

    make_log(event)
    event = json.loads(event['Messages'][0]["Body"])

    bucket_zone = event["bucket_zone"]
    key_zone = event["key_zone"]

    bucket_sample = event["bucket_sample"]
    key_sample = event["key_sample"]

    data = get_s3_data(bucket_zone, key_zone)
    data = json.loads(data)

    sample = get_s3_data(bucket_sample, key_sample)
    sample = json.loads(sample)
    paragraphs = sample["textAnnotations"]
    coordinates = (data["xmin"], data["ymin"], data["xmax"], data["ymax"])

    text = get_text_in_zone(paragraphs, coordinates)

    text = " ".join(text)

    return http_response(200, text)