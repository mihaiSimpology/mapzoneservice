import xmltodict 
import json
import os

def xml2json(file):

    filename, file_extension = os.path.splitext(file)

    if file_extension == ".xml":

        with open(file) as fx:
            data = xmltodict.parse(fx.read())
            with open(filename+".json","w") as fj:
                fj.write(json.dumps(data, indent=4))
                return True
    else:
        return False

if __name__ == "__main__":

    for file in os.listdir("data/cropped_labels"):
    
        xml2json(os.path.join("data/cropped_labels",file))

        
