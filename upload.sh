zip mapzone.zip main.py
aws s3 cp mapzone.zip s3://ml-dev.simpology2
aws lambda update-function-code --function-name "MapZoneService" --region "eu-central-1" --s3-bucket "ml-dev.simpology2" --s3-key "mapzone.zip"
aws lambda invoke --function-name "MapZoneService"  --payload '{"Messages":[{"Body":"{\"bucket_zone\":\"bucket-zone-map\", \"key_zone\":\"card number_zone.json\", \"bucket_sample\":\"google-ocr-destination\",\"key_sample\":\"test_image.json\"}"}]}' here --log-type Tail --query 'LogResult' --output text |  base64 -d
rm mapzone.zip                                                                            
cat here