import json
from main import handler



def main(event):
    handler(event, None)

if __name__ == "__main__":
    event = {"Messages":[{"Body":"{\"bucket_zone\":\"bucket-zone-map\", \"key_zone\":\"name_zone.json\", \"bucket_sample\":\"google-ocr-destination\",\"key_sample\":\"test_image.json\"}"}]}
    main(event)