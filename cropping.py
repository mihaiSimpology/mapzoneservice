from PIL import Image 
import json
import os

def crop(coordinates, image_file):

    image = Image.open(image_file)
    cropped = image.crop(coordinates)

    return cropped

def main():

    img_dir = "data/images"
    ocr_dir = "data/ocr_output"
    cro_dir = "data/cropped_images"

    for file in os.listdir(ocr_dir):

        file_path = os.path.join(ocr_dir,file)
        filename, file_extension = os.path.splitext(file)

        with open(file_path) as f:
            json_ocr = json.loads(f.read())

            point_0 = json_ocr["textAnnotations"][0]["boundingPoly"]["vertices"][0]
            point_2 = json_ocr["textAnnotations"][0]["boundingPoly"]["vertices"][2]

            coo_tuple = (point_0["x"], point_0["y"], point_2["x"], point_2["y"])

            for imagefile in os.listdir(img_dir):
                imagename,image_type = os.path.splitext(imagefile)
                if imagename == filename:
                    crop_image = crop(coo_tuple, os.path.join(img_dir,imagefile))
                    crop_image.save(os.path.join(cro_dir, imagefile))
                    break
                    

if __name__=="__main__":

    main()