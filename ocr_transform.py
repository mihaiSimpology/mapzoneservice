import matplotlib.pyplot as plt
import logging
import boto3 

import json 
import os

S3 = boto3.resource("s3")
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def put_s3_data(data, bucket, key):
    logger.info("put_s3_data function called with bucket and key:" + bucket + "/" + key)
    obj = S3.Object(bucket, key)
    return obj.put(Body=data)

def proportions_maker(dir, label):

    coordinates = [] 
    sizes = []

    for file in os.listdir(dir):

        file_path = os.path.join(dir, file)

        with open(file_path) as f:

            data = json.loads(f.read())
            data = data["annotation"] 

            sizes.append(data["size"])

            coordinates.append(list(filter(lambda x: x["name"]== label,data["object"])))

    normalized_p = []

    for i in range(len(sizes)):
        norm = {}

        place = coordinates[i][0]["bndbox"]
        size = sizes[i]

        norm["xmax"] = int(place["xmax"]) / int(size["width"])
        norm["xmin"] = int(place["xmin"]) / int(size["width"])
        norm["ymax"] = int(place["ymax"]) / int(size["height"])
        norm["ymin"] = int(place["ymin"]) / int(size["height"])

        normalized_p.append(norm)

    """
    plt.plot([x["xmax"] for x in normalized_p], label="xmax")
    plt.plot([x["ymax"] for x in normalized_p], label="ymax")
    plt.plot([x["xmin"] for x in normalized_p], label="xmin")
    plt.plot([x["ymin"] for x in normalized_p], label="ymin")
    plt.legend()
    plt.show()
    """

    mean_xmax = sum([x["xmax"] for x in normalized_p]) / len(normalized_p)
    mean_ymax = sum([x["ymax"] for x in normalized_p]) / len(normalized_p)
    mean_xmin = sum([x["xmin"] for x in normalized_p]) / len(normalized_p)
    mean_ymin = sum([x["ymin"] for x in normalized_p]) / len(normalized_p)

    zone = {"name":label,
        "xmax":mean_xmax,
        "ymax":mean_ymax,
        "xmin":mean_xmin,
        "ymin":mean_ymin
    }

    #upload to S3
    put_s3_data(json.dumps(zone), "bucket-zone-map", label+"_zone.json")



if __name__ == "__main__":

    name_list = ["name","card number", "address", "licence no", "date of birth","expiry date"]
    for name in name_list:
        proportions_maker("data/cropped_labels_json", name)